<?php

namespace pinterest;
require_once '../vendor/autoload.php';

use seregazhuk\PinterestBot\Factories\PinterestBot;
use seregazhuk\PinterestBot\Api\Forms\Registration;

class Pinterest
{
    private $auth;
    private $bot;

    public function __construct()
    {
        $this->bot = PinterestBot::create(); 
        // try
        // {
        // $this->bot->auth->login($username, $password);
        // return $this->$bot->auth->isLoggedIn;
                // } 
        // catch(\Exception $e)
        // {
                //  throw new \Exception($e->getMessage());
                // }
    }
 
      /**
     * Register a new user.
     *
     * param Registration $email
     * param string $password
     * param string $name
     */
    public function register($email,$password,$name)
    {
        try
        {
            $register = $this->bot->auth->register($email,$password,$name);
            // $response = $this->bot->auth->isLoggedIn;
            return $register;
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }


    //  login 
    public function login($username, $password)
    {
        try
        {
            $i = $this->bot->auth->login($username, $password);
            // $response = $this->bot->auth->isLoggedIn;
            return $i;
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    public function explore()
    {
        $trendingTopics = $this->bot->topics->explore();
        return json_encode($trendingTopics);
    }

    /* 
       Add users following.
       Add $username as a parameter whose friends you want follow.
       output:-increase following. 
    */
    public function followers($username)
    {
        try
        {
            $peopleToFollow = $this->bot->pinners->followers($username)->toArray();
            // print_r($peopleToFollow);
            foreach($peopleToFollow as $user) 
            {
                $this->bot->pinners->follow($user['id']);
                //  print_r($user); 
            }
            return 'done';
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }

    /*
        Unfollow your following.
        Pass username as a parameter .
        output:- Unfollow all friends 
    */
    public function following($username)
    {
        try
        {
            $peopleToFollowing = $this->bot->pinners->following($username)->toArray();
            // print_r($peopleToFollowing);
            foreach($peopleToFollowing as $user) 
            {
                $this->bot->pinners->unfollow($user['id']);
                //  print_r($user); 
            }
            return 'done';

        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /*
        Create a public board
        Pass name of board  and Description of board  in a parameter.
            
    */
    public function boardscreate($Name, $Description)
    {
        try
        {
            $boardscreate = $this->bot->boards->create($Name, $Description);
            // print_r($peopleToFollowing);
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }


    /*
        Create a private board.
        pass name of board  and Description of board  you want  in a parameter of function.
    */
    public function boardscreatePrivate($Name, $Description){
        try
        {
            $boardscreatePrivate = $this->bot->boards->createPrivate($Name, $Description);
            // print_r($peopleToFollowing);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }

    // //......options in update: 'privacy' - is public by default and 'category' 

    // public function boardsupdate($boardId,$name,$description,$privacy,$category)
    // {
    //     try
    //     {
    //         $boardsupdate=$this->bot->boards->update($boardId, [
    //                 'name'        => '$name',
    //                 'description' => '$description',
    //                 'privacy'     => '$privacy',
    //                 'category'    => '$category',
    //             ]);

    //     }
    //     catch(\Exception $e){
    //         echo "Something went wrong: ". $e->getMessage()."\n";
    //         exit(0);
    //     }
    // }

    /* 
      Delete a board 
      delete  borad throught BoardId
    */
    public function boardsdelete($boardId)
    {
        try
        {
            $boardsdelete = $this->bot->boards->delete($boardId);
            // print_r($peopleToFollowing);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }

    /*
        Get all user's boards.
        Pass username ($username) as a parameter .
    */
    public function Getboards($username)
    {
        try
        {
            $Getboards = $this->bot->boards->forUser($username);
            // print_r($Getboards);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }

    /*  
        Get Current user boards.
    */
    public function Currentboardsinfo()
    {
        try
        {
            $Currentboardsinfo = $this->bot->boards->forMe();
            // print_r($Currentboardsinfo);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }


    /* 
          Get information of a specified Board.
          pass username and board name as a parameter
    */
    public function boardsinfo($username, $board)
    {
        try
        {
            $boardsinfo = $this->bot->boards->info($username, $board);
            print_r($boardsinfo);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }
   
    /*    
        Follow board 
        Follow board by BoardId
    */
    public function boradFollow($boardId)
    {
        try
        {
            $boradFollow = $this->bot->boards->follow($boardId);
            // print_r($boradFollow);
            return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /* 
        Follow board 
        Follow board by BoardId
    */
    public function boradunFollow($boardId)
    {
        try
        {
            $boradunFollow = $this->bot->boards->unfollow($boardId);
            // print_r($boradunFollow);
            // return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }


    /*   
        Board pins.
        Get all pins for board by id.
    */
    public function boradpins($boardId)
    {
        try
        {
            foreach ($this->bot->boards->pins($boardId) as $pin) 
            {
              print_r($pin);
            }
                return 'done';
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }


    // /*..... Send board with email...*/
    // public function boradsendWithEmail($boardId,$Message,$friendemail){
    //     try
    //     {  
    //        $boradsendWithEmail= $this->bot->boards->sendWithEmail($boardId, $Message, $friendemail);
    //        return 'done'; 
    //     }
        
    //     catch(\Exception $e){
    //         echo "Something went wrong: ". $e->getMessage()."\n";
    //         exit(0);
    //     }
    // }
   
    /*  Send Board via message
        Pass Boradid , Meassage you want to be sent and Userid as a parameter.
    */
    public function boradsendWithMessage($boardId,$Message,$userId)
    {
        try
        {  
            $boradsendWithMessage= $this->bot->boards->sendWithMessage($boardId, $Message, $userId); 
            return 'done'; 
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }


    // Board Sections.

    /*
        Create section.
        Create a section for a board.
        Pass Boardid and Section name as a parameter. 
    */ 
     public function boardSections($boardId,$Section)
    {
        try
        {  
            $boardSections= $this->bot->boardSections->create($boardId,$Section); 
            return 'done'; 
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /*
        Get a list of sections for a specified boardId
    */
    public function GetboardSections($boardId)
    {
        try
        {  
            $GetboardSections= $this->bot->boardSections->forBoard($boardId);
            
            print_r($GetboardSections);
            return 'done'; 
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
  

    /*
        Update section.
        Update a section's name. You need a section id, which can be retrieved by calling $bot->boardSections->forBoard($boardId) 
    */
    // public function boardUpdatesection($sectionId,$section_name){
    //     try
    //     {  
    //        $boardUpdatesection= $this->bot->boardSections->update($sectionId,$section_name);
    //        print_r($boardUpdatesection);
    //        return 'done'; 
    //     }
        
    //     catch(\Exception $e){
    //         echo "Something went wrong: ". $e->getMessage()."\n";
    //         exit(0);
    //     }
    // }

    /*
        Delete section.
        Delete a section. You need a section id, which can be retrieved by calling $bot->boardSections->forBoard($boardId)
    */
    public function boarddeletesection($sectionId)
    {
        try
        {  
            $boarddeletesection= $this->bot->boardSections->delete($sectionId);;
        
            return 'delete'; 
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /* 
        Invites.
        Get all your active boards invites.
    */
    public function boardinvites()
    {
        try
        {  
        $boardinvites= $this->bot->boards->invites();
        print_r($boardinvites);
        
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    // Invite someone to board
    
    /*
       To a user by user id
    */
    public function sendInvite($boardId, $userId)
    {
        try
        {  
        $sendInvite= $this->bot->boards->sendInvite($boardId, $userId);
        print_r($sendInvite);
        
        }
        
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /* 
        To a user by email
        Pass Boardid and user email as a parameter.
    */
    public function sendInvite_email($boardId, $email)
    {
        try
        {  
        $sendInviteemail= $this->bot->boards->sendInvite($boardId, $email);
        print_r($sendInviteemail);
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    // Pinners

    /*
        Follow  users
        You can use both username or userid.
        Notice: When using username, bot will make one additional request to resolve user'id for his name:
    */
    public function pinnersfollow($username)
    {
        try
        {  
        $pinnersfollow= $this->bot->pinners->follow($username);
        print_r($pinnersfollow);
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

     /*
        UnFollow  users
        You can use both username or userid.
        Notice: When using username, bot will make one additional request to resolve user'id for his name:
    */
    public function pinnersunfollow($username)
    {
        try
        {  
        $pinnersunfollow= $this->bot->pinners->unfollow($username);
        return 'unfollow';
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    // User info.
    /*
        Get user info by username:
    */
    public function userData($username)
    {
        try
        {  
        $userData= $this->bot->pinners->info($username);
        //    print_r($userData);
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }


    // .......Following boards/pinners/interests
    /*
        People
        Get people the user follows.
    */
    public function followingPeople($username)
    {
        try
        {  
            foreach ($this->bot->pinners->followingPeople($username) as $user) 
            {
                print_r($user);
            }
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /*
        Boards
        Get boards the user follows
    */
    public function followingBoards($username)
    {
        try
        {  
            foreach ($this->bot->pinners->followingBoards($username) as $user) {
                // Loop through people
                print_r($user);
            }
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
    
    /*
         Get user followers .
    */
    public function Getfollowers($username)
    {
        try
        {  
            foreach ($this->bot->pinners->followers($username) as $follower) {
                print_r($follower);
            }
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }



    // /*.....Liked pins....*/

    // public function likes($username){
    //     try
    //     {  
    //         foreach ($this->bot->pinners->likes($username) as $like) {
    //             print_r($like) ;  
    //         }
    //     }
    //     catch(\Exception $e){
    //         echo "Something went wrong: ". $e->getMessage()."\n";
    //         exit(0);
    //     }
    // }

     /**
     *Get your current user's news
     */
    public function news()
    {
        try
        {
            $news = $this->bot->inbox->news()->toArray();
            foreach ($news as $new) 
            {
                $this->bot->pinners->news($user['id']);
            }
            return'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
    
   
    //  user notification
    
    /**
     * Get user info.
     * If username param is not specified, will
     * return info for logged user.
     *
     * param string $username
     * return array
     */
    public function info($username)
    {
        try
        {
            $data= $this->bot->pinners->info($username);
            return $data;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
    /**
     *Get user notification
     */
    public function notification()
    {
        try
        {
            $notifications = $this->bot->inbox->notifications()->toArray();
            foreach($notifications as $notification)
            {
                $this->bot->inbox->notifications();
            }
            return $notifications;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
        
    }
    
     /**
     * Send email.
     *
     * param string $emails
     * param string $text
     */
    public function sendEmail($emails, $text)
    {
        try
        {
            $this->bot->inbox->sendEmail($emails,$text);
            return'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
 
    /**
     * Get recommendations for query word.
     * param string $query
     * return array
     */
    public function recommendedFor($key)
    {
        try
        {
            $keywords = $this->bot->keywords->recommendedFor($key);  
            return $keywords;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }

    }
    
    /**
     * Get information of a pin by PinID.
     *
     * param string $pinId
    */ 
    public function Pininfo($pinid)
    {
        try
        {
            $info = $this->bot->pins->info($pinid); 
            return $info;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

     /**
     * Create a pin. Returns created pin info.
     * param string $imageUrl
     * param int $boardId
     * param string $description
     * param string $link
     */  
    public function createpin($imageUrl,$boardId,$description='', $link ='')
    {
        try
        {
            $this->bot->pins->create($imageUrl, $boardId, $description = '', $link = '');
            return'done';
        }    
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
    /**
     * Edit pin by ID. You can move pin to a new board by setting this board id.
     *
     * param int $pindId
     * param string $description
     * param string $link
     * param int and null $boardId
     */
    public function editpin($pinId,$description = '',$link = '',$newBoardId='')
    {
        try
        {
            $this->bot->pins->edit($pinId, $description ='', $link = '', $newBoardId='');
            
            return 'done';
        }    
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

    /**
     * move a pin to new board
     * param int $pinIds
     * param int $boardId
     */
    public function moveToBoard($pinId, $newBoardId)
    {
        try
        {
            $this->bot->pins->moveToBoard($pinId, $newBoardId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
                exit(0);
        }
    }
    /**
     * delete a pin by id 
     * pass $pinId as a paramiter
     */
    public function deletePin($pinId)
    {
        try
        { 
            $this->bot->pins->delete($pinId);
            return'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }

     /**
     * Copy pins to board
     *
     * param array|string $pinIds
     * param int $boardId
     */
    public function copyPin($pinId, $boardId)
    {
        try
        {
            $this->bot->pins->copy($pinId, $boardId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        }   
    }
    /**
     * Move pins to board
     *
     * param string $pinIds
     * param int $boardId
     */
    public function movePin($pinId, $boardId)
    {
        try
        {
            $this->bot->pins->move($pinId, $boardId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        }   
    }
     /**
     * send a pin with message 
     * pass $pinid , $message and $userId as a paramiters
     * output:- send a pin by message
     */
    public function sendWithMessage($pinId, $message, $userId)
    {
        try
        {
            $this->bot->pins->sendWithMessage($pinId, $message, $userId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * send a pin with Email
     * pass $pinid , $message and $mail as a paramiters
     * output:- send a pin by e-mail
     */
    public function sendWithEmail($pinId, $message, $email)
    {
        try
        {
            $this->bot->pins->sendWithEmail($pinId, $message, $email);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * share a link with pin 
     * pass $pinId as a paramiter
     * output:- share a pin   
     */ 
    public function share($pinId)
    {
        try
        {
            $this->bot->pins->share($pinId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Get the list of main categories
     * 
     */
    public function interests()
    {
        try
        {
            $categories = $this->bot->interests->main();
            return $categories ;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Get category info
     * param string $category
     * return array|bool
     */
    public function categoryinfo($category)
    {
        try
        {
            $info = $this->bot->interests->info($category);
            return $info ;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * follow a topic by name 
     */
    public function topicfollow($topic)
    {
        try
        {
            $this->bot->topics->follow($topic);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * unfollow a topic by name 
     */
    public function topicUnfollow($topic)
    {
        try
        {
            $this->bot->topics->unfollow($topic);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Get category info
     * paramiter string $topic
     * output:-get topic information 
    **/
    public function topicInfo($topic)
    {
        try
        {
            $topic = $this->bot->topics->info($topic);
            return $topic;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Get list of comment
     * param string $pinId
     * param int $limit
     */
    public function getList($pinId)
    {
        try
        {
            // $comments = $this->bot->comments->getList($pinId);
            $comments = $this->bot->comments->getList($pinId)->toArray();
            return $comments;
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Write a comment for a pin with current id.
     *
     * param int $pinId
     * param string $text Comment
     *
     */
    
    public function create($pinId, $text_comment)
    {
        try
        {
            $comments = $this->bot->comments->create($pinId, $text_comment);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    /**
     * Delete a comment for a pin with current id.
     * param string $pinId
     * param int $commentId
    */
    public function delete($pinId, $commentId)
    {
        try
        {
            $comments = $this->bot->comments->delete($pinId, $commentId);
            return 'done';
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    // Search suggestions
    /*
        You can get type-ahead suggestions for you search query (returns pins, pinners and boards suggestions):
    */
    public function suggestions($suggestions)
    {
        try
        {
            $suggestions = $this->bot->suggestions->searchFor($suggestions);;
            // return $suggestions;
            print_r($suggestions);
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }

    /*
        Tags suggestions.
        You can get type-ahead suggestions for tags (returns tags and pins counts for these tags).
        There is no need to prepend your query with # symbol.
    */
    public function Tagsuggestions($Tagsuggestions)
    {
        try
        {
            $Tagsuggestions = $this->bot->suggestions->tagsFor($Tagsuggestions);
            // return $suggestions;
            print_r($Tagsuggestions);
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    
    /*
        Search only in my pins
        you search your pins  if you are logged in.
    */
    public function searchInMyPins($Tagsuggestions)
    {
        try
        {
            $searchInMyPins = $this->bot->pins->searchInMyPins($Tagsuggestions)->toArray();
            return $searchInMyPins;
            print_r($searchInMyPins);
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }
    // Search in pins
    public function pinsearch($Tagsuggestions)
    {
        try
        {
            $pinsearch = $this->bot->pins->search($Tagsuggestions)->toArray();;
            return $pinsearch;
          
        }
        catch(\Exception $e)
        {
            echo "Something went wrong: ". $e->getmessage()."\n";
            exit(0);
        } 
    }



    /*
        Search in pinners
        Pinterest allows to search for pinners only if you are logged in
        Search in people
    */
    public function pinnersearch($tag)
    {
        try
        {  
            foreach($this->bot->pinners->search($tag) as $pinner) {
            print_r($pinner);
            }
        }
        catch(\Exception $e){
            echo "Something went wrong: ". $e->getMessage()."\n";
            exit(0);
        }
    }
}

